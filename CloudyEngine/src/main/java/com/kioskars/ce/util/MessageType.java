package com.kioskars.ce.util;

public enum MessageType {
	INITIALIZATION,
	RUNNING,
	END,
	LOAD,
	CREATION,
	DELETION,
	RETRIEVING,
	PROPERTIES,
	
	;
}
