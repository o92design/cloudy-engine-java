package com.kioskars.ce.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;


// Brought to you by: http://stackoverflow.com/questions/2942857/how-to-convert-current-date-into-string-in-java

public class Time{

	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	
	public static final String now(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}
	
}
