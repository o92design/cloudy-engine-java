package com.kioskars.ce.util;

import java.util.Vector;

import com.kioskars.ce.core.EngineManager;


//TODO(KiOskars): Look up if MINNESS LÄCKA!!

public class Debug {
	enum DebugType{
		ERROR,
		SUCCESSFUL,
		STATUS,
		INFO;
	}
	public static final Vector<String> log = new Vector<String>();
	
	private static abstract class Type extends Debug{
		protected static DebugType debugType;
		private static DebugType getDebugType() {
			return debugType;
		}
		
		// Just print out the message to console
		@SafeVarargs
		protected static <T> String[] print(MessageType type, T...messages){
			// Create error message
			String debugTypeString = getDebugType().toString() + " -> " + type.toString() + " |";
			
			// Create new messages array
			String[] msgs = new String[messages.length + 1];
			msgs[0] = debugTypeString;			
			for(int i = 1; i < msgs.length; i++){
				msgs[i] = messages[i - 1].toString();
			}
			print(msgs);
			return msgs;
		}
		
		//Logging the message to the log vector
		@SafeVarargs
		protected static <T> String[] log(MessageType type, T...messages){
			// Create error message
			String debugTypeString = getDebugType().toString() + " -> " + type.toString() + " |";
			
			// Create new messages array
			String[] msgs = new String[messages.length + 1];
			msgs[0] = debugTypeString;			
			for(int i = 1; i < msgs.length; i++){
				msgs[i] = messages[i - 1].toString();
			}
			log(msgs);
			return msgs;
		}
	}

	public static class Error extends Type{
		@SafeVarargs
		public static <T> String[] print(MessageType type, T...messages){
			debugType = DebugType.ERROR;
			return Type.print(type, messages);
		}
		@SafeVarargs
		public static <T> String[] log(MessageType type, T...messages){
			debugType = DebugType.ERROR;
			return Type.log(type, messages);
		}
	}
	
	public static class Successful extends Type{
		@SafeVarargs
		public static <T> String[] print(MessageType type, T...messages){
			debugType = DebugType.SUCCESSFUL;
			return Type.print(type, messages);
		}
		@SafeVarargs
		public static <T> String[] log(MessageType type, T...messages){
			debugType = DebugType.SUCCESSFUL;
			return Type.log(type, messages);
		}
	}
	
	public static class Status extends Type{
		@SafeVarargs
		public static <T> String[] print(MessageType type, T...messages){
			debugType = DebugType.STATUS;
			return Type.print(type, messages);
		}
		@SafeVarargs
		public static <T> String[] log(MessageType type, T...messages){
			debugType = DebugType.STATUS;
			return Type.log(type, messages);
		}
	}
	public static class Info extends Type{
		@SafeVarargs
		public static <T> String[] print(MessageType type, T...messages){
			debugType = DebugType.INFO;
			return Type.print(type, messages);
		}
		@SafeVarargs
		public static <T> String[] log(MessageType type, T...messages){
			debugType = DebugType.INFO;
			return Type.log(type, messages);
		}
	}
	
	//TODO(KiOskars): Look into if MINNESS LÄCKA!
	@SafeVarargs
	public static <T> String print(T...messages){		
		String message = combineMessages(messages);
		if(EngineManager.debug)
			System.out.println(message);
		
		return message;
	}
	
	@SafeVarargs
	public static <T> String log(T...messages){
		String message = print(messages);
		addToLog(message);
		return message;
	}
	
	static void addToLog(String message){
		log.add(message);
	}
	
	@SafeVarargs
	static <T> String combineMessages(T...messages){
		String message = Time.now() + " | ";
		
		int i = 0;
		while(i < messages.length){
			message += messages[i].toString() + " ";
			i++;
		}
		return message;
	}
}
