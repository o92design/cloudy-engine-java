package com.kioskars.ce.util.observer;

public class ObserverMessage{
	private int id; // 1 == run
	private String info;
	
	public ObserverMessage(int id, String info){
		this.id = id;
		this.info = info;
	}
	
	public final int getId(){
		return id;
	}
	
	public final void setId(int id){
		this.id = id;
	}
	
	public final String getInfo() {
		return info;
	}

	public final void setInfo(String info) {
		this.info = info;
	}

}
