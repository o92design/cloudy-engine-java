package com.kioskars.ce.physics;

import java.awt.Shape;

public abstract class Collider {

	private ColliderType type;
	protected int x, y;
	protected int localX = 0, localY = 0;
	protected int width, height;
	
	public Collider(ColliderType type, int localX, int localY, int width, int height){
		this.type = type;
		
		this.localX = localX;
		this.localY = localY;
		this.width = width;
		this.height = height;
	}
	
	public void refresh(boolean p_leftDirection, int p_posX, int p_posY){
		//setSize(parent.properties.spriteWidth, parent.properties.spriteHeight);
		setPosition(p_leftDirection, p_posX, p_posY);
	}
	
	public abstract void setPosition(boolean p_leftDirection, int p_posX, int p_posY);
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	
	public abstract void setSize(int width, int height);
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	
	public abstract <T  extends Shape> T getShape();
	public abstract <T  extends Shape> void setShape(T shape);

	public final ColliderType getType() {
		return type;
	}

	public final void setType(ColliderType type) {
		this.type = type;
	}

	public final int getLocalX() {
		return localX;
	}

	public final void setLocalX(int localX) {
		this.localX = localX;
	}

	public final int getLocalY() {
		return localY;
	}

	public final void setLocalY(int localY) {
		this.localY = localY;
	}
}
