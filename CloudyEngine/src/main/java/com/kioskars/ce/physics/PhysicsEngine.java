package com.kioskars.ce.physics;

import com.kioskars.ce.core.system.Subsystem;

public class PhysicsEngine extends Subsystem {

	private CollisionManager collisionManager;
	private int windowWidth = 640, windowHeight = 480;
	
	public boolean initialize(String name, int ID) {
		setName(name);
		setID(ID);
		setCollisionManager(new CollisionManager());
		return true;
	}

	public CollisionManager getCollisionManager() {
		return collisionManager;
	}

	public void setCollisionManager(CollisionManager collisionManager) {
		this.collisionManager = collisionManager;
	}
	
	public void addCollider(Collider col){
		try{
			collisionManager.colliders.add(col);
			System.out.println("Lade till collider i colliders");
		}catch(Exception e){
			System.err.printf("Kunde inte lägga in collider i colliders |\n%s", e.toString());
			e.printStackTrace();
		}
	}

	public int getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(int windowWitdh) {
		this.windowWidth = windowWitdh;
	}

	public int getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(int windowHeight) {
		this.windowHeight = windowHeight;
	}

}
