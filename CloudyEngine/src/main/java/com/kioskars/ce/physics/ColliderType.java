package com.kioskars.ce.physics;

import java.awt.Color;

public enum ColliderType {
	HEAD(Color.blue),
	BODY(Color.yellow),
	FEET(Color.red),
	WEAPON(Color.green);
	
	private final Color color;
	private ColliderType(Color color){
		this.color = color;
	}
	
	public final Color getColor(){
		return color;
	}
}
