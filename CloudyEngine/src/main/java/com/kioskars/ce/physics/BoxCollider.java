package com.kioskars.ce.physics;

import java.awt.Rectangle;
import java.awt.Shape;

public class BoxCollider extends Collider{
	
	private Rectangle rect;
	
	public BoxCollider(ColliderType type, int localX, int localY, int width, int height) {
		super(type, localX, localY, width, height);
		rect = new Rectangle();
		
		rect.setBounds(this.localX, this.localY, this.width, this.height);
	}
	
	public void setPosition(boolean p_leftDirection, int p_posX, int p_posY){
		int posX = 0;
		int posY = 0;

		if(p_leftDirection)
		{
			posX = (p_posX - localX / 2);
			posY = (p_posY + localY);			
		}
		else
		{
			posX = (p_posX + localX);
			posY = (p_posY + localY);	
		}

		
		x = posX;
		y = posY;
		
		rect.setLocation(x, y);
	}
	
	public void setSize(int width, int height){
		this.width = width;
		this.height = height;
		rect.setSize(this.width, this.height);
	}
	public int getWidth(){
		return rect.width;
	}
	public int getHeight(){
		return rect.height;
	}

	public final Rectangle getRect() {
		return rect;
	}

	public final void setRect(Rectangle rect) {
		this.rect = rect;
	}

	public <T extends Shape> T getShape() {
		return (T) rect;
	}

	public <T  extends Shape> void setShape(T shape) {
		rect = (Rectangle) shape;
	}

}
