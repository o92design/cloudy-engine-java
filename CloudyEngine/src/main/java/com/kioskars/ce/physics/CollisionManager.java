package com.kioskars.ce.physics;

import java.util.ArrayList;

public class CollisionManager {
	
	public ArrayList<Collider> colliders = new ArrayList<Collider>();

	public boolean checkColliderIntersect(Collider left, Collider right){
		if(left.getShape().intersects(right.getShape()))
			return true;
		else
			return false;
	}
}
