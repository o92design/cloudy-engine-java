package com.kioskars.ce.entity;

public enum EntityTypes {
	Default,
	Player,
	Enemy,
	World
}
