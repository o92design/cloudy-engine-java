package com.kioskars.ce.entity;

import java.util.ArrayList;
import java.util.Collections;

import com.kioskars.ce.components.Component;
import com.kioskars.ce.components.ComponentId;
import com.kioskars.ce.components.PhysicsComponent;
import com.kioskars.ce.core.system.Subsystem;
import com.kioskars.ce.physics.Collider;
import com.kioskars.ce.physics.CollisionManager;
import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class EntityManager extends Subsystem{

	private ArrayList<Entity> entities;
	private CollisionManager collisionManager;
	
	public EntityManager(CollisionManager collisionManager){
		this.collisionManager = collisionManager;
	}

	public boolean initialize(String name, int ID) {
		entities = new ArrayList<Entity>();
		return false;
	}
	
	public void sort(){
		Collections.sort(entities);
	}
	
	public int size(){
		return entities.size();
	}
	
	public void addEntity(Entity ent){
		try{
			entities.add(ent);
			sort();
		}catch(Exception e){
			Debug.Error.log(MessageType.CREATION, "Kunde inte lägga in entity", e.toString());
			e.printStackTrace();
		}
	}
	
	public void removeEntity(Entity ent){
		try{
			ArrayList<Component> phList = ent.getComponents(ComponentId.PHYSICS);
			if(phList != null){
				for(int i = 0; i < phList.size(); ++i){
					PhysicsComponent c = (PhysicsComponent) phList.get(i);
					for(Collider col : c.getColliders()){
						collisionManager.colliders.remove(col);
					}
				}
			}
			entities.remove(ent);
		}catch(Exception e){
			Debug.Error.log(MessageType.DELETION, "Kunde inte ta bort entity", e.toString());
			e.printStackTrace();
		}
	}
	public void removeEntity(int index){
		try{
			entities.remove(index);
		}catch(Exception e){
			Debug.Error.log(MessageType.DELETION, "Kunde inte ta bort entity", e.toString());
			e.printStackTrace();
		}
	}
	
	public void removeAllEntities(){
		try{
			entities.removeAll(entities);
		}catch(Exception e){
			Debug.Error.log(MessageType.DELETION, "Kunde inte ta bort in entities", e.toString());
			e.printStackTrace();
		}
	}
	public Entity getEntity(int index){
		Entity ent = null;
		try {
			ent = entities.get(index);
		} catch (Exception e) {
			Debug.Error.log(MessageType.RETRIEVING, "Kunde inte hämta entity", e.toString());
			e.printStackTrace();
		}
		return ent;
	}

	public final ArrayList<Entity> getEntities() {
		return entities;
	}

	public final void setEntities(ArrayList<Entity> entities) {
		this.entities = entities;
	}
}
