package com.kioskars.ce.entity;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.util.Debug;

public abstract class EntityState {

	public ComponentMessageEvent stateID;
	public Entity entity;
	
	public EntityState(Entity entity){
		this.entity = entity;
	}
	protected void onEnter(){
		Debug.print(entity.properties.name + " gick in i " + stateID.name() + " state.");
		entity.state = this;
		ComponentMessage message = new ComponentMessage(stateID, entity.properties.name + " är i " + stateID.name() + " state.", entity);
		entity.send(message);
	}
	public abstract void update(float dt);	
	protected void onExit(){
		Debug.print(entity.properties.name + " gick ur " + stateID.name() + " state.");
	}
}
