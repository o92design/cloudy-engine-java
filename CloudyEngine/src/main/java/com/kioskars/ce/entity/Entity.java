package com.kioskars.ce.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeMap;

import com.kioskars.ce.components.Component;
import com.kioskars.ce.components.ComponentId;
import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.PropertiesComponent;
import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public abstract class Entity implements Comparator<Entity>, Comparable<Entity>{
	
	protected boolean active = true;
	public EntityState state;
	public PropertiesComponent properties = new PropertiesComponent("properties", ComponentId.PROPERTIES, this);
	
	private TreeMap<ComponentId, ArrayList<Component>> componentsMap = new TreeMap<ComponentId, ArrayList<Component>>();
	
	// Hämtar input och fysik komponenter och kallar komponentens update metod.
	public void update(float dt){
		state.update(dt);
	}
	
	// Hämtar komponenter med samma id och kallar på dess update metod.
	public void updateComponents(ComponentId ID){
		ArrayList<Component> comps = getComponents(ID);
		
		for(int i = 0; i < comps.size(); ++i){
			comps.get(i).update();
		}
	}
	
	// Updaterar utvalda komponenter. 
	public void updateComponents(boolean sort, ComponentId...ID){
		if(sort)
			Arrays.sort(ID);
		
		for(int index = 0; index < ID.length; ++index){
			ArrayList<Component> comps = getComponents(ID[index]);
			
			for(int i = 0; i < comps.size(); ++i){
				comps.get(i).update();
			}
		}
		
	}
	
	// Updaterar alla komponenter.
	public void updateComponents(){
		updateComponents(true, ComponentId.INPUT, ComponentId.PHYSICS, ComponentId.PROPERTIES);
	}
	
	// Byter state genom att gå ur det nuvarande.
	public void changeState(EntityState newState){
		if(newState == null)
			return;
		
		state.onExit();
		state = newState;
		state.onEnter();
		
	}
	
	// Itererar och skickar meddelandet till komponenter. Ifall komponentId är null så skickas det till alla.
	public void send(ComponentMessage message, ComponentId...components){
		if(components == null){
			send(message);
		}
		else{
			for(int i = 0; i < components.length; ++i){
				for(Component c : getComponents(components[i])){
					c.receive(message);
				}
			}
		}
	}
	
	public void send(ComponentMessage message){
		for(ArrayList<Component> cList : componentsMap.values()){
			for(Component c : cList)
				c.receive(message);
		}
	}
	
	// Lägger in en ny komponent lista i hashmappens komponetens id plats.
	public void addComponent(Component component){
		ArrayList<Component> comps = getComponents(component.getID());
		comps.add(component);
		try{
		componentsMap.put(component.getID(), comps);		
		}catch(Exception e){
			Debug.Error.log(MessageType.CREATION, "Kunde inte lägga in komponent:", e.toString());
			e.printStackTrace();
			return;
		}
		
		Debug.Successful.log(MessageType.CREATION, "Lade till " + component.getID().toString() + " komponent till", properties.name);
		Debug.Status.print(MessageType.PROPERTIES, properties.name + "s komponenttyper: " +  componentsMap.keySet().toString());
	}
	
	// Hämtar alla komponenter till en lista med samma komponent id
	public ArrayList<Component> getComponents(ComponentId ID){
		ArrayList<Component> comps = null;
		try{
			comps = componentsMap.get(ID);
		}catch(Exception e){
			Debug.Error.log(MessageType.RETRIEVING, "Kunde inte hämta komponenter:", e.toString());
			e.printStackTrace();
		}
		if(comps == null)
			comps = new ArrayList<Component>();
		return comps;
	}
	
	public int getNrOfComponents(){
		int number = 0;
		for(ArrayList<Component> componentsList : componentsMap.values()){
			number += componentsList.size();
		}
		
		return number;
	}
	
	public int compare(Entity e, Entity e1){
		
		return e.properties.y + e.properties.spriteHeight - e1.properties.y + e1.properties.spriteHeight;
	}
	
	public int compareTo(Entity o) {
		Integer e = this.properties.y + this.properties.spriteHeight;
		Integer e1 = o.properties.y + o.properties.spriteHeight;
		return e.compareTo(e1);
	}
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}


}
