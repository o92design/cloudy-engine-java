package com.kioskars.ce.core;

import com.kioskars.ce.games.helga.HelgaGame;

public class Engine {
	
	public static void main(String[] args){
		EngineManager engine = new EngineManager(new HelgaGame("Helga by KiOskars", 640, 480));
		
		if(engine.initialize())
		{
			engine.run();
		}
		
		System.exit(0);
	}

}
