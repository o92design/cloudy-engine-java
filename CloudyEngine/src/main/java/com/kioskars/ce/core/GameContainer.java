package com.kioskars.ce.core;

import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class GameContainer implements Runnable{

	private Thread thread;
	private AbstractGame game;
	private EngineManager engine;
	
	private double frameCap = 1.0 / 60.0;

	private volatile boolean running;
	
	public GameContainer(EngineManager engine, AbstractGame game){
		running = false;
		this.engine = engine;
		this.game = game;
	}
	
	
	private void render(){
		engine.getGraphicsEngine().getRenderer().clear();
		engine.getGameStateMachine().render();
		engine.getGraphicsEngine().getRenderer().show();
	}
	
	public void run() {
		running = true;
		
		double firstTime = 0;
		double lastTime = System.nanoTime() / 1000000000.0;
		double passedTime = 0;
		double unprocessedTime = 0;
		double frameTime = 0;
		int frames = 0;
		int fps = 0;
		
		while(running)
		{
			boolean render = false;
			
			firstTime = System.nanoTime() / 1000000000.0;
			passedTime = firstTime - lastTime;
			lastTime = firstTime;
			
			unprocessedTime += passedTime;
			frameTime += passedTime;
			
			while(unprocessedTime >= frameCap)
			{
				engine.getInputManager().update(engine);
				engine.getGameStateMachine().update((float)frameCap);
				unprocessedTime -= frameCap;
				render = true;
				
				if(frameTime >= 1)
				{
					if(EngineManager.debug) Debug.print("FPS: " + fps);
					frameTime = 0;
					fps = frames;
					frames = 0;
				}
			}
			
			if(render)
			{	
				render();
				
				frames++;
			}
			else
			{
				try
				{
					Thread.sleep(5);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
		
		stop();
	}
	public synchronized void start(){
		if(running)
			return;
		
		game.init(this);
		
		thread = new Thread(this);
		thread.run();
	}
	
	public synchronized void stop(){
		if(!running)
			return;
		
		running = false;
		engine.getGraphicsEngine().cleanUp();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Debug.Error.log(MessageType.END, e.toString());
			e.printStackTrace();
		}
	}


	public EngineManager getEngine() {
		return engine;
	}

}
