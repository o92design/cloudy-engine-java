package com.kioskars.ce.core;

import java.awt.Color;
import java.util.Vector;

import com.kioskars.ce.core.system.Subsystem;
import com.kioskars.ce.entity.EntityManager;
import com.kioskars.ce.game.GameStateMachine;
import com.kioskars.ce.gfx.GraphicsEngine;
import com.kioskars.ce.input.InputManager;
import com.kioskars.ce.physics.PhysicsEngine;
import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;
import com.kioskars.ce.world.WorldManager;

public class EngineManager {

	private Vector<Subsystem> subsystemsList;
	
	private InputManager inputManager;
	private PhysicsEngine physicsEngine;
	private GraphicsEngine graphicsEngine;
	private GameStateMachine gameStateMachine;
	private EntityManager entityManager;
	private WorldManager worldManager;
	private GameContainer gc;
	
	public static boolean debug = false;
	private AbstractGame game;
	
	public EngineManager(AbstractGame game){
		this.game = game;
	}
	
	
	public boolean initialize(){
		
		subsystemsList = new Vector<Subsystem>();

		inputManager = new InputManager();
		initializeSubSystem("InputManager", 1, inputManager);
		
		physicsEngine = new PhysicsEngine();
		initializeSubSystem("PhysicsEngine", 2, physicsEngine);
		physicsEngine.setWindowWidth(game.getWidth());
		physicsEngine.setWindowHeight(game.getHeight());
		
		graphicsEngine = new GraphicsEngine();
		initializeSubSystem("GraphicsEngine", 3, graphicsEngine);
		graphicsEngine.createWindow(game.getTitle(), game.getWidth(), game.getHeight(), false, Color.black);
		graphicsEngine.getWindow().addKeyListener(inputManager.getKeyboard());
		
		entityManager = new EntityManager(physicsEngine.getCollisionManager());
		initializeSubSystem("EntityManager", 4, entityManager);
		
		worldManager = new WorldManager();
		initializeSubSystem("WorldManager", 5, worldManager);
		
		
		gameStateMachine = new GameStateMachine(this);
		initializeSubSystem("GameStateMachine", 5, gameStateMachine);
		gc = new GameContainer(this, game);
		return true;
	}
	
	private boolean initializeSubSystem(String name, int ID, Subsystem subsystem){
		
		if(subsystem.initialize(name, ID))
			addSubsystem(subsystem);
		else
			return false;
		
		return true;
	}
	
	private boolean addSubsystem(Subsystem subsystem){
		try{
			subsystemsList.add(subsystem);
		}catch(Exception e){
			Debug.Error.print(MessageType.CREATION, "Kunde inte lägga in " + subsystem.getName() + " till motorns subsystem.");
			Debug.Info.print(MessageType.CREATION, e.toString());
			return false;
		}
		Debug.Successful.print(MessageType.CREATION, "Lade till " + subsystem.getName() + " till motorns subsystem");
		return true;
	}
	
	public boolean run(){
		gc.start();
		return true;
	}

	public GraphicsEngine getGraphicsEngine() {
		return graphicsEngine;
	}


	public InputManager getInputManager() {
		return inputManager;
	}


	public final GameStateMachine getGameStateMachine() {
		return gameStateMachine;
	}


	public final EntityManager getEntityManager() {
		return entityManager;
	}


	public final PhysicsEngine getPhysicsEngine() {
		return physicsEngine;
	}
	
	public final WorldManager getWorldManager() {
		return worldManager;
	}
}
