package com.kioskars.ce.core.system;

public abstract class Subsystem {

	private String name;
	private int ID;
	
	public abstract boolean initialize(String name, int ID);
	public void printInfo(){
	}

	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public int getID() {
		return ID;
	}

	protected void setID(int ID) {
		this.ID = ID;
	}
	
}
