package com.kioskars.ce.core;

public abstract class AbstractGame {
	private String title;
	private int height;
	private int width;
	public AbstractGame(String title, int width, int height){ this.title = title; this.width = width; this.height = height;}
	
	public abstract void init(GameContainer gc);

	String getTitle() {
		return title;
	}

	void setTitle(String title) {
		this.title = title;
	}

	int getHeight() {
		return height;
	}

	void setHeight(int height) {
		this.height = height;
	}

	int getWidth() {
		return width;
	}

	void setWidth(int width) {
		this.width = width;
	}
}
