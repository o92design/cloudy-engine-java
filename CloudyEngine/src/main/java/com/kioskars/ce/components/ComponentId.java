package com.kioskars.ce.components;

public enum ComponentId {
	INPUT(0),
	PHYSICS(1),
	PROPERTIES(2),
	
	// This shall always be last!
	GRAPHICS(5);
	
	private final int value;
	private ComponentId(int value){
		this.value = value;
	}
	
	public final int getValue(){
		return value;
	}
}

