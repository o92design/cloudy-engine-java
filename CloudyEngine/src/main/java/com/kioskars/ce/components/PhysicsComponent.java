package com.kioskars.ce.components;

import java.util.Vector;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityTypes;
import com.kioskars.ce.physics.Collider;
import com.kioskars.ce.physics.PhysicsEngine;

public abstract class PhysicsComponent extends Component {

	private PhysicsEngine physics;
	protected Vector<Collider> colliders;

	protected PhysicsComponent(String tag, Entity entity, PhysicsEngine physics) {
		super(tag, ComponentId.PHYSICS, entity);
		this.setPhysics(physics);
		colliders = new Vector<Collider>();
	}
	
	public abstract void update();
	public abstract void receive(ComponentMessage message);

	public void addCollider(EntityTypes entityType, Collider col){
		colliders.add(col);
		physics.addCollider(col);
	}
	public Vector<Collider> getColliders(){
		return colliders;
	}
	
	public void refreshColliders(){
		borderCollision(physics.getWindowWidth(), physics.getWindowHeight());
		
		for(int i = 0; i < colliders.size(); ++i){
			colliders.get(i).refresh(entity.properties.leftDirection, entity.properties.x, entity.properties.y);
		}
	}
	
	public void borderCollision(int borderWidth, int borderHeight){
		
		int correction = 10; // Korregerar dimenisoner pga JPanel?
		// Sätter entitien innanför borderns bredd
		if(getEntity().properties.x <= 0)
			getEntity().properties.x = 0;
		if(getEntity().properties.x >= borderWidth - getEntity().properties.spriteWidth + correction)
			getEntity().properties.x = borderWidth - getEntity().properties.spriteWidth + correction;
		
		// Sätter entitien innanför borderns höjd
		if(getEntity().properties.y <= 0)
			getEntity().properties.y = 0;
		if(getEntity().properties.y >= borderHeight - getEntity().properties.spriteHeight + correction)
			getEntity().properties.y = borderHeight - getEntity().properties.spriteHeight + correction;
	}
	
	public PhysicsEngine getPhysics() {
		return physics;
	}

	public void setPhysics(PhysicsEngine physics) {
		this.physics = physics;
	}

}
