package com.kioskars.ce.components;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.util.observer.ObserverMessage;

public class ComponentMessage extends ObserverMessage {

	private ComponentId componentID;
	private ComponentMessageEvent event;
	private Entity sender;
	
	public ComponentMessage(ComponentMessageEvent event, String info, Entity sender) {
		super(event.getValue(), info);
		this.setComponentID(componentID);
		this.event = event;
		this.setSender(sender);
	}

	public ComponentId getComponentID() {
		return componentID;
	}

	public void setComponentID(ComponentId componentID) {
		this.componentID = componentID;
	}

	public ComponentMessageEvent getEvent() {
		return event;
	}

	public void setEvent(ComponentMessageEvent event) {
		this.event = event;
	}

	public Entity getSender() {
		return sender;
	}

	public void setSender(Entity sender) {
		this.sender = sender;
	}

}
