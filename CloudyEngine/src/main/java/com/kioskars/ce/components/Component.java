package com.kioskars.ce.components;

import java.util.Comparator;

import com.kioskars.ce.entity.Entity;

public abstract class Component implements Comparator<Component>, Comparable<Component> {
	protected String tag = null;
	protected final ComponentId ID;
	protected Entity entity;
	
	protected Component(String tag, final ComponentId ID, Entity entity) { this.tag = tag; this.ID = ID; this.entity = entity; }
	
	public abstract void update();
	public abstract void receive(ComponentMessage message);	
	
	public String getTag(){ return tag; }
	public void setTag(String tag){ this.tag = tag; }
	public final ComponentId getID(){ return ID; } 
	
	public int compareTo(Component c){
		return (this.ID).compareTo(c.ID);
	}
	
	public int compare(Component c, Component c1){
		return c.ID.getValue() - c1.ID.getValue();
	}

	public final Entity getEntity() {
		return entity;
	}

	public final void setEntity(Entity entity) {
		this.entity = entity;
	}
}
