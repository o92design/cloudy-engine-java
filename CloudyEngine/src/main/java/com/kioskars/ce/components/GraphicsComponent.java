package com.kioskars.ce.components;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.gfx.Renderer;
import com.kioskars.ce.gfx.Sprite;

public abstract class GraphicsComponent extends Component{
	
	protected Renderer r;
	protected Sprite origSprite;
	protected Sprite drawingSprite;
	
	protected GraphicsComponent(String tag, Entity entity, Renderer r) {
		super(tag, ComponentId.GRAPHICS, entity);
		this.r = r;
	}

	public abstract void update();
	
	public Sprite getSprite(){
		return origSprite;
	}
	public void setSprite(Sprite sprite){
		this.drawingSprite = sprite;
		entity.properties.spriteWidth = sprite.getWidth();
		entity.properties.spriteHeight = sprite.getHeight();
	}
}
