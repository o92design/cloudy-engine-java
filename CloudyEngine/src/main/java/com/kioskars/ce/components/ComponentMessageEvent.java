package com.kioskars.ce.components;

public enum ComponentMessageEvent {
	Idle(0),
	Walk(1),
	Attack(2),
	Damage(3);
	
	private final int value;
	
	private ComponentMessageEvent(int value){
		this.value = value;
	}
	
	public final int getValue(){
		return value;
	}
	
}
