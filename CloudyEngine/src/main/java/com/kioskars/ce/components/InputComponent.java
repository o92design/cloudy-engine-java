package com.kioskars.ce.components;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.input.InputManager;

public abstract class InputComponent extends Component {
	
	protected InputManager input;

	protected InputComponent(String tag, Entity entity, InputManager input) {
		super(tag, ComponentId.INPUT, entity);
		this.input = input;
	}
	
	public abstract void update();
}
