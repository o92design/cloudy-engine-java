package com.kioskars.ce.components;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityTypes;
import com.kioskars.ce.util.Debug;

public class PropertiesComponent extends Component{
	
	public EntityTypes type = EntityTypes.Default;
	public String name = "Entity " + type.toString();
	public String tag = "null";
	public int health;
	public int x = 0;
	public int y = 0;
	public float velocityX;
	public float velocityY;
	
	// Graphics properties
	public int spriteWidth = 0;
	public int spriteHeight = 0;
	public int scaleX = 1, scaleY = 1;
	public boolean leftDirection = false;
	
	
	public PropertiesComponent(String tag, ComponentId ID, Entity entity) {
		super(tag, ComponentId.PROPERTIES, entity);
	}

	@Override
	public void update() {
	}

	@Override
	public void receive(ComponentMessage message) {
		if(message.getEvent() == ComponentMessageEvent.Damage && message.getSender() != entity){
			health -= 20;
			Debug.print(name + " helath: " + health);
			if(health <= 0){
				Debug.print(name + " died...");
				entity.setActive(false);				
			}
		}
	}

}
