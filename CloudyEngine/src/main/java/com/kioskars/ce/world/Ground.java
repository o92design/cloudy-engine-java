package com.kioskars.ce.world;

import com.kioskars.ce.gfx.Sprite;
import com.kioskars.ce.physics.BoxCollider;

public abstract class Ground {
	protected GroundType type;
	protected int x, y;
	protected int width, height;
	
	protected Sprite sprite;
	protected BoxCollider collider;
	
	public Ground(Sprite sprite, int x, int y){
		this.sprite = sprite;
		this.x = x;
		this.y = y;
	}
	
	public Ground(Ground copyGround){
		this.type = copyGround.type;
		
		this.x = copyGround.x;
		this.y = copyGround.y;
		
		this.width = copyGround.width;
		this.height = copyGround.height;
		
		this.sprite = copyGround.sprite;
		this.collider = copyGround.collider;
	}
	
}
