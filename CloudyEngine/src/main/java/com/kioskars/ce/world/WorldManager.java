package com.kioskars.ce.world;

import java.util.Vector;

import com.kioskars.ce.core.system.Subsystem;
import com.kioskars.ce.gfx.Renderer;
import com.kioskars.ce.gfx.SpriteSheet;
import com.kioskars.ce.gfx.Window;
import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class WorldManager extends Subsystem{

	private Vector<Ground> groundTiles;

	public boolean initialize(String name, int ID) {
		setName(name);
		setID(ID);
		groundTiles = new Vector<Ground>();
		return true;
	}
	
	public void renderGround(Window win, Renderer r){
		if(groundTiles.isEmpty())
			return;

		for(int x = 0; x <= win.getWidth() + 64; x += 64){
			for(int y = 0; y <= win.getHeight() + 64; y += 64){
				Grass g = new Grass(groundTiles.firstElement());
				g.x = x;
				g.y = y;
				r.drawSprite(g.sprite, g.x, g.y, false, 1, 1, null);
			}
		}
	}
	
	public Ground createGroundTile(GroundType type, SpriteSheet sheet, int sheetX, int sheetY, int x, int y){
		Ground g = null;
		
		switch(type){
		case Grass:
			g = new Grass(sheet.crop(sheetX, sheetY, 64, 64, false), x, y);
			break;
		default:
			break;
		}
		
		if(g == null){
			Debug.Error.log(MessageType.CREATION, "Kunde inte skapa tile");
		}
		else{
			groundTiles.add(g);
		}
		
		return g;
	}
}
