package com.kioskars.ce.world;

import com.kioskars.ce.gfx.Sprite;

public class Grass extends Ground{
	public Grass(Sprite sprite, int x, int y){
		super(sprite, x, y);
		type = GroundType.Grass;
		width = height = 64;
	}
	public Grass(Ground copyGrass){
		super(copyGrass);
	}
}
