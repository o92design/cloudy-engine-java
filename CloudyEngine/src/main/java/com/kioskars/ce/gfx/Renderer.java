package com.kioskars.ce.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.TreeSet;

import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class Renderer{
	private Window window;
	private BufferedImage bfImage;
	private BufferStrategy bufferStrategy;
	private Graphics graphics;
	private Color drawColor;
	private int width, height;
	
	private TreeSet<Sprite> spriteTree;
	
	public Renderer(Window window, Color drawColor){
		this.window = window;
		bfImage = new BufferedImage(window.getHeight(), window.getWidth(), BufferedImage.TYPE_INT_RGB);
		this.drawColor = drawColor;
		
		width = window.getFrame().getWidth();
		height = window.getFrame().getHeight();
		
		window.setRenderer(this);
		
		spriteTree = new TreeSet<Sprite>();
	}
	
	public void sendSpriteToRender(Sprite sprite){
		try{
			spriteTree.add(sprite);
		}catch(Exception e){
			Debug.Error.log(MessageType.RUNNING, "Kunde inte lägga in sprite i renderTree");
			e.printStackTrace();
		}
	}
	
	public void drawRect(Rectangle rect, Color color){
		graphics.setColor(color);
		//graphics.fillRect(rect.x, rect.y, rect.width, rect.height);
		graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
	}
		
	public void drawImage(BufferedImage image, int x, int y, boolean flipImage, int scaleX, int scaleY, Color transDrawColor){
		if(flipImage)
		{
			graphics.drawImage(image,
							   x + image.getWidth() * scaleX, y, x, y + image.getHeight() * scaleY, 
							   0, 0, image.getWidth(), image.getHeight(),
							   transDrawColor, null);
		}
		else{
			graphics.drawImage(image, 
							   x, y, x+image.getWidth() * scaleX, y+image.getHeight() * scaleY,
							   0, 0, image.getWidth(), image.getHeight(),
							   transDrawColor, null);
		}
	}
	public void drawSprite(Sprite sprite, int x, int y, boolean flipSprite, int scaleX, int scaleY, Color transDrawColor){
		drawImage(sprite.getSprite(), x, y, flipSprite, scaleX, scaleY, transDrawColor);
	}
	
	public void clear(){
		bufferStrategy = window.getCanvas().getBufferStrategy();
		if(bufferStrategy == null){
			Debug.print("Canvas was null");
			window.getCanvas().createBufferStrategy(window.getBUFFER_SIZE());
		}
		graphics = bufferStrategy.getDrawGraphics();
		
		// Background
		graphics.setColor(drawColor);
		graphics.fillRect(0, 0, width, height);
	}
	
	
	
	public void show(){
		bufferStrategy.show();
		graphics.dispose();
	}

	
	public void cleanUp(){
		graphics.dispose();
		bufferStrategy.dispose();
		bfImage.flush();
	}
	

	BufferedImage getBfImage() {
		return bfImage;
	}

	BufferStrategy getBufferStrategy() {
		return bufferStrategy;
	}

	Color getDrawColor() {
		return drawColor;
	}

	public Graphics getGraphics() {
		return graphics;
	}

	
	
}
