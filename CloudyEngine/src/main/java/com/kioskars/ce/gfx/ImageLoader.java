package com.kioskars.ce.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class ImageLoader {
	public static BufferedImage loadImage(String path){
		BufferedImage image = null;
		try {
			image = ImageIO.read(ImageLoader.class.getResource(path));
			Debug.Successful.log(MessageType.LOAD, "Lade till " + path);
		} catch (IOException e) {
			Debug.Error.log(MessageType.LOAD, e.toString(), "path är null");
			e.printStackTrace();
		}
		return image;
	}
}
