package com.kioskars.ce.gfx;

import java.awt.image.BufferedImage;

public class Sprite {
	private BufferedImage sprite;
	private int sheetX, sheetY;
	private int width, height;
	private float pivotCenterX;
	private float pivotCenterY;
	
	public Sprite(BufferedImage sprite, int sheetX, int sheetY, int width, int height){
		this.sprite = sprite;
		this.sheetX = sheetX;
		this.sheetY = sheetY;
		this.width = width;
		this.height = height;
		
		pivotCenterX = width * 0.5f; 
		pivotCenterY = height * 0.5f; 
	}

	public BufferedImage getSprite() {
		return sprite;
	}

	public void setSprite(BufferedImage sprite) {
		this.sprite = sprite;
	}

	public int getSheetX() {
		return sheetX;
	}

	public void setSheetX(int sheetX) {
		this.sheetX = sheetX;
	}

	public int getSheetY() {
		return sheetY;
	}

	public void setSheetY(int sheetY) {
		this.sheetY = sheetY;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public float getPivotCenterX() {
		return pivotCenterX;
	}

	public void setPivotCenterX(float pivotCenterX) {
		this.pivotCenterX = pivotCenterX;
	}

	public float getPivotCenterY() {
		return pivotCenterY;
	}

	public void setPivotCenterY(float pivotCenterY) {
		this.pivotCenterY = pivotCenterY;
	}
	
}
