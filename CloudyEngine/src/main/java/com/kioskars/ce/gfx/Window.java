package com.kioskars.ce.gfx;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class Window {
	private JFrame frame;
	private Canvas canvas;
	
	private Renderer renderer;
	
	private String title;
	private int width;
	private int height;
	private boolean resizable;
	private final int BUFFER_SIZE = 3;
	
	public Window(String title, 
			int width, int height, 
			boolean resizable){
		
		this.title = title;
		this.width = width;
		this.height = height;
		this.resizable = resizable;	
		
		if(initializeWindow()){
			Debug.Successful.log(MessageType.CREATION, "Lyckades skapa ett fönster");
		}
		else
		{
			Debug.Error.log(MessageType.CREATION, "Lyckades inte skapa ett fönster");
		}
		printInfoProperties();
	}
	
	private void printInfoProperties(){
		Debug.Info.print(MessageType.PROPERTIES, "Fönster:", title, width, height);
	}
	
	private boolean initializeWindow(){
		return initializeCanvas() ? (initializeFrame() ? true : false) : false;
	}
	
	private boolean initializeCanvas(){
		try {
			canvas = new Canvas();
			Dimension d = new Dimension(width, height);
			canvas.setSize(d);
			canvas.setPreferredSize(d);
			canvas.setMaximumSize(d);
			canvas.setMinimumSize(d);
			canvas.setFocusable(false);
			
			Debug.Successful.log(MessageType.CREATION, "Lyckades skapa en canvas");
			Debug.Status.log(MessageType.PROPERTIES, "Canvas: " + canvas.getSize().getWidth() + " | " + canvas.getSize().getHeight());
		} catch (Exception e) {
			e.printStackTrace();
			Debug.Error.log(MessageType.CREATION, e.toString());
			Debug.Error.log(MessageType.CREATION, "Lyckades inte skapa en canvas");
			return false;
		}
		
		
		return true;
	}
	
	private boolean initializeFrame(){
		try {
			frame = new JFrame(title);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLayout(new BorderLayout());
			
			frame.add(canvas);
			frame.pack();
			frame.setLocationRelativeTo(null);

			frame.setVisible(true);
			frame.setFocusable(true);
			frame.setResizable(resizable);
			
			canvas.createBufferStrategy(BUFFER_SIZE);
			
			Debug.Successful.log(MessageType.CREATION, "Lyckades skapa en frame");
			Debug.Status.log(MessageType.PROPERTIES, "Frame: " + frame.getWidth() + " | " + frame.getHeight());
		} catch (HeadlessException e) {

			Debug.Error.log(MessageType.CREATION, e.toString());
			Debug.Error.log(MessageType.CREATION, "Lyckades inte skapa en frame");
			return false;
		}
		
		return true;
	}
	
	public boolean addKeyListener(KeyListener keyboard){
		
		boolean addedKeyListener = false;
		try{
			frame.addKeyListener(keyboard);
			addedKeyListener = true;
			Debug.Successful.log(MessageType.LOAD, "Laddade in keyboard som listener till " + this);
		}catch(Exception e){
			Debug.Error.log(MessageType.LOAD, e.toString());
			e.printStackTrace();
		}
		return addedKeyListener;
	}
	
	public void cleanUp(){
		renderer.cleanUp();
		frame.dispose();
	}

	public boolean isResizable() {
		return resizable;
	}

	public void setResizable(boolean resizable) {
		this.resizable = resizable;
	}

	public JFrame getFrame() {
		return frame;
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title){
		this.title = title;
		frame.setTitle(this.title);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public final int getBUFFER_SIZE() {
		return BUFFER_SIZE;
	}

	public Renderer getRenderer() {
		return renderer;
	}

	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}
}
