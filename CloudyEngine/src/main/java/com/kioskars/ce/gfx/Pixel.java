package com.kioskars.ce.gfx;

public class Pixel {
	
	// Going for ARGB
	
	// A pixel is built up by bits 0xffdc326e represented by hex values
	// not sure what 0x represents, it can be the bit size?
	
	// ff is Alpha channel = 100 % opacity
	// dc represents the red channel = 86 % red
	// 32 represents the blue channel = 20 % blue
	// 6e represents the green channel = 43 % green
	
	public static float getAlpha(int color)
	{
		// I bitwise the int 24 bits to get from the blue channel to the Alpha channel
		return (0xff & ( color >> 24)) / 255f;
	}
	
	public static float getRed(int color)
	{
		// I bitwise the int 16 bits to get from the blue channel to the Red channel
		return (0xff & ( color >> 16)) / 255f;
	}
	
	public static float getGreen(int color)
	{
		// I bitwise the int 8 bits to get from the blue channel to the Green channel
		return (0xff & ( color >> 8)) / 255f;
	}
	
	public static float getBlue(int color)
	{
		// I don't bitwise because I'm in the Blue channel
		return (0xff & ( color)) / 255f;
	}
	
	public static int getColor(float a, float r, float g, float b)
	{
		return ((int)(a * 255f + 0.5f) << 24 |
				(int)(r * 255f + 0.5f) << 16 |
				(int)(g * 255f + 0.5f) << 8  |
				(int)(b * 255f + 0.5f));
	}
	
	public static int getColorPower(int color, float power)
	{
		return getColor(1, getRed(color) * power,
						   getGreen(color) * power,
						   getBlue(color) * power);
	}
	
	public static int getMax(int c0, int c1)
	{
		return getColor(1, Math.max(getRed(c0), getRed(c1)),
				   		   Math.max(getGreen(c0), getGreen(c1)),
				   		   Math.max(getBlue(c0), getBlue(c1)));
	}
}
