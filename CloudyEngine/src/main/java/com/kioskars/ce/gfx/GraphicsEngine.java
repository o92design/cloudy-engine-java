package com.kioskars.ce.gfx;

import java.awt.Color;

import com.kioskars.ce.core.system.Subsystem;
import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class GraphicsEngine extends Subsystem{
	
	Window window;
	Renderer renderer;

	public Window createWindow(String title,
			int width, int height, 
			boolean resizable, Color drawColor){
	
		window = new Window(title, width, height, resizable);
		
		createRenderer(window, drawColor);
		return window;
	}
	private Renderer createRenderer(Window window, Color drawColor){
		
		renderer = new Renderer(window, drawColor);
		
		return renderer;
	}

	@Override
	public boolean initialize(String name, int ID) {
		setName(name);
		setID(ID);
		Debug.Status.log(MessageType.INITIALIZATION, "Initierar Grafikmotorn");
		return true;
	}
	
	public boolean cleanUp(){
		window.cleanUp();
		return true;
	}

	public Window getWindow() {
		return window;
	}

	public Renderer getRenderer() {
		return renderer;
	}
	
}
