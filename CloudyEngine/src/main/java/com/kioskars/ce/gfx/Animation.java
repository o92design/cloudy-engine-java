package com.kioskars.ce.gfx;

public class Animation{

	private String name;
	private int currentFrame;
	private Sprite[] frames;
	private int playbackSpeed;
	
	public boolean run = true;
	
	private long lastTime, timer;
	
	public Animation(String name, Sprite[] sprites, int playbackSpeed){
		this.setName(name);
		
		frames = sprites;
		this.playbackSpeed = playbackSpeed;
		
		currentFrame = 0;
		timer = 0;
		
		lastTime = System.currentTimeMillis();
		
	}
	
	public void playAnimation(){
		if(!run)
			return;
		
		timer += System.currentTimeMillis() - lastTime;
		lastTime = System.currentTimeMillis();
		
		if(timer > playbackSpeed * 100){
			currentFrame++;
			timer = 0;
			
			if(currentFrame >= frames.length)
				currentFrame = 0;
		}
	}
	
	public void resetAnimation(){
		currentFrame = 0;
	}
	
	public boolean onLastFrame(){
		return currentFrame == frames.length - 1;
	}
	
	public Sprite getCurrentFrame(){
		return frames[currentFrame];
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
