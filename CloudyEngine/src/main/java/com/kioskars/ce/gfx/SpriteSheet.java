package com.kioskars.ce.gfx;

import java.awt.image.BufferedImage;

import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class SpriteSheet {

	private BufferedImage sheet;
	private String name;
	private final int DEFUALT_SPRITE_SIZE = 64;
	
	public SpriteSheet(BufferedImage image, String name){
		this.name = name;
		this.sheet = image;
	}
	
	public Sprite crop(int x, int y, int width, int height, boolean defaultTileSize){
		Sprite sprite = null;
		try {
			if(defaultTileSize)
				sprite = new Sprite(sheet.getSubimage(x * DEFUALT_SPRITE_SIZE, y * DEFUALT_SPRITE_SIZE, width, height), x, y, width, height);
			else
				sprite = new Sprite(sheet.getSubimage(x, y, width, height), x, y, width, height);
			Debug.Successful.log(MessageType.CREATION, "Skapade sprite från " + name);
		} catch (Exception e) {
			Debug.Error.log(MessageType.CREATION, "Kunde inte skapa sprite", e.toString());
			e.printStackTrace();
		}
		return sprite;
	}
}
