package com.kioskars.ce.games.helga.movement;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.entity.Entity;

public class MoveUpCommand extends MovementCommands {
	public MoveUpCommand(int velocity) {
		super(velocity);
	}

	public void execute(Entity entity) {
		super.execute(entity);
		entity.properties.velocityY = -getVelocity();
		entity.send(new ComponentMessage(ComponentMessageEvent.Walk, entity.properties.name + " walk up", entity));
	}
	
}
