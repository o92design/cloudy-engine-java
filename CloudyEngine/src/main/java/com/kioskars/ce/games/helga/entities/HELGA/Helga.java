package com.kioskars.ce.games.helga.entities.HELGA;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityTypes;

public class Helga extends Entity{
	
	public Helga(String tag,
				int health,
				int x, int y, 
				boolean active,
				boolean leftDirection,
				int scaleX, int scaleY){
		properties.type = EntityTypes.Player;
		
		properties.tag = tag;
		properties.health = health;
		properties.x = x;
		properties.y = y;
		
		addComponent(properties);
	}	
}
