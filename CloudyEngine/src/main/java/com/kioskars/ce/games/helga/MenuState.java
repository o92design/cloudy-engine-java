package com.kioskars.ce.games.helga;

import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.game.GameState;
import com.kioskars.ce.game.GameStateId;
import com.kioskars.ce.util.Debug;

public class MenuState extends GameState{

	public MenuState(GameStateId ID, EngineManager engine){
		super(ID, engine);
	}
	
	public boolean onEnter() {
		Debug.print("Entering MenuState");
		return true;
	}


	public boolean update(float dt) {
		
		return true;
	}
	
	public boolean render() {
		// TODO Auto-generated method stub
		return false;
	}


	public boolean onExit() {
		Debug.print("Exiting MenuState");
		return true;
	}

	

}
