package com.kioskars.ce.games.helga.entities.HELGA;

import java.awt.event.KeyEvent;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.InputComponent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.games.helga.movement.MoveDownCommand;
import com.kioskars.ce.games.helga.movement.MoveLeftCommand;
import com.kioskars.ce.games.helga.movement.MoveRightCommand;
import com.kioskars.ce.games.helga.movement.MoveUpCommand;
import com.kioskars.ce.input.InputManager;

public class HelgaInputComponent extends InputComponent {

	public int keyCode_MoveUp = KeyEvent.VK_W;
	public int keyCode_MoveDown = KeyEvent.VK_S;
	public int keyCode_MoveLeft = KeyEvent.VK_A;
	public int keyCode_MoveRight = KeyEvent.VK_D;
	
	public int keyCode_Attack = KeyEvent.VK_R;	
	
	private MoveUpCommand moveUp;
	private MoveDownCommand moveDown;
	private MoveLeftCommand moveLeft;
	private MoveRightCommand moveRight;
	
	private AttackCommand attack;
	
	private int speed = 3;
	
	public HelgaInputComponent(String tag, Entity entity, InputManager input) {
		super(tag, entity, input);
		
		moveUp = new MoveUpCommand(speed);
		moveDown = new MoveDownCommand(speed);
		moveLeft = new MoveLeftCommand(speed);
		moveRight = new MoveRightCommand(speed);
		
		attack = new AttackCommand();
	}
	
	public void setKeys(int keyUp, int keyDown, int keyLeft, int keyRight){
		keyCode_MoveUp = keyUp;
		keyCode_MoveDown = keyDown;
		keyCode_MoveLeft = keyLeft;
		keyCode_MoveRight = keyRight;
	}

	@Override
	public void update() {		
		handleInput();
	}
	
	public void handleInput(){

		if(entity.properties.velocityX != 0){
			entity.properties.velocityX = 0;
		}
		if(entity.properties.velocityY != 0){
			entity.properties.velocityY = 0;
		}
		
		if(input.getKeyboard().isKey(keyCode_MoveUp)) moveUp.execute(entity);
		if(input.getKeyboard().isKey(keyCode_MoveDown)) moveDown.execute(entity);
		if(input.getKeyboard().isKey(keyCode_MoveLeft)) moveLeft.execute(entity);
		if(input.getKeyboard().isKey(keyCode_MoveRight)) moveRight.execute(entity);

		if(input.getKeyboard().isKeyPressed(keyCode_Attack)){
			attack.execute(entity);
		}
		
	}

	public final int getSpeed() {
		return speed;
	}

	public final void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public void receive(ComponentMessage message) {
		// TODO Auto-generated method stub
		
	}

}
