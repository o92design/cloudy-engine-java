package com.kioskars.ce.games.helga.entities.ENEMIES;

import com.kioskars.ce.components.ComponentId;
import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.components.GraphicsComponent;
import com.kioskars.ce.components.PhysicsComponent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.physics.BoxCollider;
import com.kioskars.ce.physics.ColliderType;
import com.kioskars.ce.physics.PhysicsEngine;

public class EnemyPhysicsComponent extends PhysicsComponent{
	
	public EnemyPhysicsComponent(String tag, Entity entity, PhysicsEngine physics) {
		super(tag, entity, physics);
		
		GraphicsComponent gpC = (GraphicsComponent) entity.getComponents(ComponentId.GRAPHICS).get(0);
		
		BoxCollider feet = new BoxCollider(ColliderType.FEET, 0, gpC.getSprite().getHeight() - 20, gpC.getSprite().getWidth(), 20);
		colliders.add(feet);
		physics.addCollider(feet);
		
		BoxCollider body = new BoxCollider(ColliderType.BODY, 0, 22, gpC.getSprite().getWidth(), 86);
		colliders.add(body);
		physics.addCollider(body);
		
		BoxCollider head = new BoxCollider(ColliderType.HEAD, 0, 2, gpC.getSprite().getWidth(), 20);
		colliders.add(head);
		physics.addCollider(head);
		
		BoxCollider wpn = new BoxCollider(ColliderType.WEAPON, 20 + gpC.getSprite().getWidth(), 40, 20, 45);
		colliders.add(wpn);
		physics.addCollider(wpn);
	}

	@Override
	public void update() {
		refreshColliders();
	}

	@Override
	public void receive(ComponentMessage message) {
		if(message.getEvent() == ComponentMessageEvent.Damage){
			takeDamage(message.getSender(), 20);
		}
	}
	
	public void takeDamage(Entity player, int knockback){
		if(player.properties.x <= getEntity().properties.x)
			getEntity().properties.x += knockback;
		else
			getEntity().properties.x -= knockback;
	}

}
