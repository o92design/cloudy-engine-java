package com.kioskars.ce.games.helga;

import com.kioskars.ce.core.AbstractGame;
import com.kioskars.ce.core.GameContainer;
import com.kioskars.ce.game.GameStateId;

public class HelgaGame extends AbstractGame {

	public HelgaGame(String title, int width, int height) {
		super(title, width, height);
		// This should be empty!
	}

	public void init(GameContainer gc) {
		gc.getEngine().getGameStateMachine().changeState(new PlayState(GameStateId.PLAY_STATE, gc.getEngine()));
	}
	
}
