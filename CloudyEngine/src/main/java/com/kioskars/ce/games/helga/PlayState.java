package com.kioskars.ce.games.helga;

import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.game.GameState;
import com.kioskars.ce.game.GameStateId;
import com.kioskars.ce.games.helga.EntityStates.IdleState;
import com.kioskars.ce.games.helga.entities.HELGA.Helga;
import com.kioskars.ce.games.helga.entities.HELGA.HelgaGraphicsComponent;
import com.kioskars.ce.games.helga.entities.HELGA.HelgaInputComponent;
import com.kioskars.ce.games.helga.entities.HELGA.HelgaPhysicsComponent;
import com.kioskars.ce.games.helga.entities.WORLD.*;
import com.kioskars.ce.gfx.ImageLoader;
import com.kioskars.ce.gfx.SpriteSheet;
import com.kioskars.ce.util.Debug;
import com.kioskars.ce.world.GroundType;

public class PlayState extends GameState{

	public PlayState(GameStateId ID, EngineManager engine){
		super(ID, engine);
	}

	public boolean onEnter() {
		
		Debug.print("Entering PlayState");
		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/SpriteSheet.png"), "SpriteSheet");
		engine.getWorldManager().createGroundTile(GroundType.Grass, sheet, 192, 446, 0, 0);
		
		Helga player = new Helga("player", 100, 320, 240, true, false, 1, 1);
		player.state = new IdleState(player);

		SpriteSheet helgaSheet = new SpriteSheet(ImageLoader.loadImage("/Helga2.png"), "helgaSheet");
		player.addComponent(new HelgaGraphicsComponent(helgaSheet, "graphics", player, engine.getGraphicsEngine().getRenderer()));
		player.addComponent(new HelgaInputComponent("input", player, engine.getInputManager()));
		player.addComponent(new HelgaPhysicsComponent("physics", player, engine.getPhysicsEngine()));
		engine.getEntityManager().addEntity(player);

		TreeStump stump = new TreeStump("world", 320, 240, true, false, 1, 1);
		stump.addComponent(new StaticObjectGraphicsComponent(sheet, stump, 
															engine.getGraphicsEngine().getRenderer(),
															193, 126,
															64, 64,
															false));
		stump.addComponent(new StaticObjectPhysicsComponent(stump, engine.getPhysicsEngine(), 64, 32));
		stump.state = new IdleState(stump);
		engine.getEntityManager().addEntity(stump);


		TreeStump stump2 = new TreeStump("world", 120, 100, true, false, 1, 1);
		stump2.addComponent(new StaticObjectGraphicsComponent(sheet, stump2, 
															engine.getGraphicsEngine().getRenderer(),
															193, 126,
															64, 64,
															false));
		stump2.addComponent(new StaticObjectPhysicsComponent(stump2, engine.getPhysicsEngine(), 64, 32));
		stump2.state = new IdleState(stump2);
		engine.getEntityManager().addEntity(stump2);
	
		return true;
	}


	public boolean update(float dt) {
		updateEntities(dt);
		return true;
	}
	
	
	public boolean render() {
		engine.getWorldManager().renderGround(engine.getGraphicsEngine().getWindow(), engine.getGraphicsEngine().getRenderer());
		renderEntities();
		
		if(EngineManager.debug) 
			renderColliders();
		
		return true;
	}


	public boolean onExit() {
		Debug.print("Exiting PlayState");
		engine.getEntityManager().removeAllEntities();
		return true;
	}

	

}
