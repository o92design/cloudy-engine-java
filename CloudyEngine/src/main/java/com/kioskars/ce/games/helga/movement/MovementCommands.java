package com.kioskars.ce.games.helga.movement;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.games.helga.EntityStates.WalkState;
import com.kioskars.ce.input.Command;

public abstract class MovementCommands implements Command {
	
	private int velocity;
	
	public MovementCommands(int velocity){
		this.velocity = velocity;
	}
	public void execute(Entity entity){
		entity.changeState(new WalkState(entity));
	}
	
	public final int getVelocity() {
		return velocity;
	}
	public final void setVelocity(int velocity) {
		this.velocity = velocity;
	}
}
