package com.kioskars.ce.games.helga.entities.HELGA;

import com.kioskars.ce.components.ComponentId;
import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.input.Command;

public class AttackCommand implements Command {

	@Override
	public void execute(Entity entity) {

		entity.send(new ComponentMessage(ComponentMessageEvent.Attack, entity.properties.name + " attacks!", entity), ComponentId.PHYSICS, ComponentId.GRAPHICS);
	}
	
	
}
