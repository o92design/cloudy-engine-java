package com.kioskars.ce.games.helga.movement;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.entity.Entity;

public class MoveLeftCommand extends MovementCommands {
	public MoveLeftCommand(int velocity) {
		super(velocity);
	}

	public void execute(Entity entity) {
		if(!entity.properties.leftDirection)
			entity.properties.leftDirection = true;
		
		super.execute(entity);
		entity.properties.velocityX = -getVelocity();	
		entity.send(new ComponentMessage(ComponentMessageEvent.Walk, entity.properties.name + " walk left", entity));
	}

}
