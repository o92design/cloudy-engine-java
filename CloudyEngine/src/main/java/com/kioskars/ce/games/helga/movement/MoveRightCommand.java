package com.kioskars.ce.games.helga.movement;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.entity.Entity;

public class MoveRightCommand extends MovementCommands {
	public MoveRightCommand(int velocity) {
		super(velocity);
	}

	public void execute(Entity entity) {
		if(entity.properties.leftDirection)
			entity.properties.leftDirection = false;
		
		super.execute(entity);
		entity.properties.velocityX = getVelocity();
		entity.send(new ComponentMessage(ComponentMessageEvent.Walk, entity.properties.name + " walk right", entity));
	}

}
