package com.kioskars.ce.games.helga.entities.HELGA;

import com.kioskars.ce.components.ComponentId;
import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.components.GraphicsComponent;
import com.kioskars.ce.components.PhysicsComponent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.physics.BoxCollider;
import com.kioskars.ce.physics.Collider;
import com.kioskars.ce.physics.ColliderType;
import com.kioskars.ce.physics.PhysicsEngine;
import com.kioskars.ce.util.Debug;

public class HelgaPhysicsComponent extends PhysicsComponent {

	public HelgaPhysicsComponent(String tag, Entity entity, PhysicsEngine physics) {
		super(tag, entity, physics);
		
		GraphicsComponent gpC = (GraphicsComponent) entity.getComponents(ComponentId.GRAPHICS).get(0);
		
		BoxCollider feet = new BoxCollider(ColliderType.FEET, 0, gpC.getSprite().getHeight() - 20, gpC.getSprite().getWidth(), 20);
		colliders.add(feet);
		
		BoxCollider body = new BoxCollider(ColliderType.BODY, 0, 22, gpC.getSprite().getWidth(), 86);
		colliders.add(body);
		
		BoxCollider head = new BoxCollider(ColliderType.HEAD, 0, 2, gpC.getSprite().getWidth(), 20);
		colliders.add(head);
		
		BoxCollider wpn = new BoxCollider(ColliderType.WEAPON, 20 + gpC.getSprite().getWidth(), 40, 20, 45);
		colliders.add(wpn);
	}

	public void update() {
		movement();
	}
	// Needs to be reworked... checking collision to much
	public void movement(){
		int lastPosX = entity.properties.x;
		int lastPosY = entity.properties.y;
		
		Collider collidedObject = checkCollisions(colliders.get(0), ColliderType.BODY);
		if(collidedObject != null){
			int objectLeftSide = collidedObject.getLocalX();
			int objectRightSide = collidedObject.getLocalX() + collidedObject.getWidth() - 4;
			
			int objectUpSide = collidedObject.getLocalY() - collidedObject.getHeight() ;
			int objectDownSide = collidedObject.getLocalY() ;
			
			int knockback = 5;

			/*
			System.out.println("Collided with object");
			System.out.println("Last Pos X: " + lastPosX + 
			" Last Pos Y: " + lastPosY +
			"| objectRightSide:" + objectRightSide +
			"| objectLeftSide:" + objectLeftSide +
			"| objectUpSide:" + objectUpSide +
			"| objectDownSide:" + objectDownSide);
			*/

			if(lastPosX <= objectLeftSide){
				entity.properties.x -= knockback;
			} else if(lastPosX >= objectRightSide){
				entity.properties.x += knockback;
			}
			
			//TODO(#10): Refactor for taking size into consideration
			/*
			if(lastPosY <= objectUpSide){
				entity.properties.y -= knockback;
				System.out.println("UPSIDE!");
			}
			if(lastPosY >= objectDownSide){
				entity.properties.y += knockback;
				System.out.println("DOWNSIDE!");
			} 
			*/
		}
			
		entity.properties.x += entity.properties.velocityX;
		entity.properties.y += entity.properties.velocityY;
		
		
		if(entity.properties.velocityX == 0 && entity.properties.velocityY == 0)
			entity.send(new ComponentMessage(ComponentMessageEvent.Idle, entity.properties.name + " is idle", entity));
		
		refreshColliders();
	}
	
	private Collider checkCollisions(Collider collider, ColliderType otherColliderType){
		Collider otherCol = null;
		
		for(int i = 0; i < getPhysics().getCollisionManager().colliders.size(); ++i){
			if(getPhysics().getCollisionManager().colliders.get(i).getType() == otherColliderType){
				otherCol = getPhysics().getCollisionManager().colliders.get(i); 
				if(getPhysics().getCollisionManager().checkColliderIntersect(collider, otherCol)){
					return otherCol;
				}
			}
		}
		
		return null;
	}

	@Override
	public void receive(ComponentMessage message) {
		Debug.print(message.getInfo());
		
		switch(message.getEvent()){
		case Idle:
			
			break;
		case Walk:

			break;
		case Attack:
			//check if wpn (collider index 3) collider collide with enemyCol
			Collider enemyCol = checkCollisions(colliders.get(3), ColliderType.BODY);
			if(enemyCol != null){
				ComponentMessage dmgMessage = new ComponentMessage(ComponentMessageEvent.Damage, 
																   getEntity().properties.name + "'s " + colliders.get(3).getType() + 
																   " kolliderade med " + enemyCol, getEntity());
				//TODO(KiOskars): Workout how to get specific entity - enemyCol.getParent().send(dmgMessage, ComponentId.PROPERTIES, ComponentId.PHYSICS, ComponentId.GRAPHICS);
				entity.send(dmgMessage);
			}
			break;
		case Damage:
			
			break;
		default:
			
			
			break;
		
		}		
	}

}
