package com.kioskars.ce.games.helga.entities.WORLD;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.PhysicsComponent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityTypes;
import com.kioskars.ce.physics.BoxCollider;
import com.kioskars.ce.physics.ColliderType;
import com.kioskars.ce.physics.PhysicsEngine;

public class StaticObjectPhysicsComponent extends PhysicsComponent {

    public StaticObjectPhysicsComponent(Entity entity, PhysicsEngine physics, int width, int height) {
        super("physics", entity, physics);
        BoxCollider body = new BoxCollider(ColliderType.BODY, entity.properties.x, entity.properties.y + height, width, height);
		colliders.add(body);
        physics.addCollider(body);
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void receive(ComponentMessage message) {
        // TODO Auto-generated method stub
        
    }
    
}
