package com.kioskars.ce.games.helga.entities.ENEMIES;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityTypes;

public class Enemy extends Entity{
	public Enemy(int health, int x, int y){
		
		properties.type = EntityTypes.Enemy;
		properties.health = health;
		properties.y = y;
		properties.x = y;
		
		addComponent(properties);
	}
}
