package com.kioskars.ce.games.helga.EntityStates;

import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityState;

public class WalkState extends EntityState {

	public WalkState(Entity entity) {
		super(entity);
		this.stateID = ComponentMessageEvent.Walk;
	}

	@Override
	protected void onEnter() {
		super.onEnter();
	}

	@Override
	public void update(float dt) {
		entity.updateComponents();
	}

	@Override
	protected void onExit() {
		super.onExit();
	}
	
}
