package com.kioskars.ce.games.helga.EntityStates;

import com.kioskars.ce.components.ComponentMessageEvent;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityState;


public class IdleState extends EntityState{
	
	public IdleState(Entity entity){
		super(entity);
		stateID = ComponentMessageEvent.Idle;
	}
	
	@Override
	protected void onEnter() {
		super.onEnter();
	}

	@Override
	public void update(float dt) {
		entity.updateComponents();
	}

	@Override
	protected void onExit() {
		super.onExit();
	}

}
