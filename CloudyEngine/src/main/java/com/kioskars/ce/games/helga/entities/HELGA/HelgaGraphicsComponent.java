package com.kioskars.ce.games.helga.entities.HELGA;

import java.awt.Color;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.GraphicsComponent;
import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.gfx.Animation;
import com.kioskars.ce.gfx.Renderer;
import com.kioskars.ce.gfx.Sprite;
import com.kioskars.ce.gfx.SpriteSheet;
import com.kioskars.ce.util.Debug;

public class HelgaGraphicsComponent extends GraphicsComponent{
	private SpriteSheet sheet;
	private Animation currentAnimation;
	
	private Animation[] animations; //0.Idle 1.Walk 2.Attack
	
	public HelgaGraphicsComponent(SpriteSheet helgaSheet, String tag, Entity entity, Renderer r) {
		super(tag, entity, r);
		this.sheet = helgaSheet;
		origSprite = sheet.crop(0, 0, 48, 128, false);
		setSprite(origSprite);
		
		animations = new Animation[3];
		
		// Idle animation
		createAnimation("Idle", 0, 2, 0, 10);
		
		// Create walk Animation
		createAnimation("Walk", 1, 2, 128, 2);
		
		// Attack animation
		createAnimation("Attack", 2, 4, 256, 1);
	}
	
	public void update() {
		
		if(currentAnimation == null)
			drawingSprite = origSprite;
		else{
			currentAnimation.playAnimation();
			drawingSprite = currentAnimation.getCurrentFrame();	
			Debug.print("Playing " + currentAnimation.getName() + " animation");
		}

		
		if(!EngineManager.debug)
			r.drawSprite(drawingSprite, entity.properties.x, entity.properties.y, entity.properties.leftDirection, entity.properties.scaleX, entity.properties.scaleY, null);
		else
			r.drawSprite(drawingSprite, entity.properties.x, entity.properties.y, entity.properties.leftDirection, entity.properties.scaleX, entity.properties.scaleY, Color.pink);
			
	}

	@Override
	public void receive(ComponentMessage message) {
		
		switch(message.getEvent()){
		case Idle:
			currentAnimation = animations[0];
			break;
		case Walk:
			currentAnimation = animations[1];
			break;
		case Attack:
			currentAnimation = animations[2];
			break;
		case Damage:
			currentAnimation = animations[0];
			break;
		default:
			currentAnimation = animations[0];
			
			break;
		}
	}
	
	private void createAnimation(String name, int index, int length, int yPos, int playbackSpeed){
		Sprite[] sprites = new Sprite[length];
		for(int i = 0; i < sprites.length; i++){
			sprites[i] = sheet.crop(i * 48, yPos, 48, 128, false);
		}
		
		animations[index] = new Animation(name, sprites, playbackSpeed);
		
	}
}
