package com.kioskars.ce.games.helga.entities.WORLD;

import java.awt.Color;

import com.kioskars.ce.components.ComponentMessage;
import com.kioskars.ce.components.GraphicsComponent;
import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.gfx.Renderer;
import com.kioskars.ce.gfx.SpriteSheet;

public class StaticObjectGraphicsComponent extends GraphicsComponent{
    private SpriteSheet sheet;

    public StaticObjectGraphicsComponent(SpriteSheet worldSheet, 
                                        Entity entity, 
                                        Renderer r,
                                        int xCrop, int yCrop,
                                        int width, int height,
                                        boolean defaultTileSize) {
        super("graphics", entity, r);
        this.sheet = worldSheet;
        origSprite = sheet.crop(xCrop, yCrop, width, height, defaultTileSize);
        setSprite(origSprite);
        drawingSprite = origSprite;
    }

    @Override
    public void update() {
        if(!EngineManager.debug)
			r.drawSprite(drawingSprite, entity.properties.x, entity.properties.y, entity.properties.leftDirection, entity.properties.scaleX, entity.properties.scaleY, null);
		else
			r.drawSprite(drawingSprite, entity.properties.x, entity.properties.y, entity.properties.leftDirection, entity.properties.scaleX, entity.properties.scaleY, Color.PINK);
    }

    @Override
    public void receive(ComponentMessage message) {
        // TODO Auto-generated method stub
        
    }
}
