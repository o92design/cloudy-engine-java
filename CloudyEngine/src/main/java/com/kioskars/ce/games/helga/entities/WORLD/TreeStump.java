package com.kioskars.ce.games.helga.entities.WORLD;

import com.kioskars.ce.entity.Entity;
import com.kioskars.ce.entity.EntityTypes;

public class TreeStump extends Entity{
    
    public TreeStump(String tag,
                    int x, int y,
                    boolean active,
                    boolean lefDirection,
                    int scaleX, int scaleY) {
        properties.type = EntityTypes.World;
        properties.tag = tag;
        properties.health = 100;
        properties.x = x;
        properties.y = y;
        
        addComponent(properties);
    }
}
