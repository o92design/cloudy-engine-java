package com.kioskars.ce.game;

public enum GameStateId {
	MENU_STATE,
	PLAY_STATE,
	END_STATE,
}
