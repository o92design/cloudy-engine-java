package com.kioskars.ce.game;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.kioskars.ce.components.Component;
import com.kioskars.ce.components.ComponentId;
import com.kioskars.ce.components.PhysicsComponent;
import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.entity.Entity;

public abstract class GameState {
	
	protected GameStateId ID;
	protected EngineManager engine;
	
	protected GameState(GameStateId ID, EngineManager engine){
		this.ID = ID;
		this.engine = engine;
	}
	
	public abstract boolean onEnter();
	public abstract boolean update(float dt);
	public abstract boolean render();
	public abstract boolean onExit();
	
	public boolean renderEntities(){
		engine.getEntityManager().sort();
		
		ArrayList<Entity> entities = engine.getEntityManager().getEntities();
		for(int i = 0; i < entities.size(); ++i){
			entities.get(i).updateComponents(ComponentId.GRAPHICS);
		}
		return true;
	}
	
	public boolean renderColliders(){
		ArrayList<Component> comps = getEntityComponents(ComponentId.PHYSICS);
		PhysicsComponent c;
		Rectangle rect;
		for(int i = 0; i < comps.size(); ++i){
			c = (PhysicsComponent) comps.get(i);
			for(int k = 0; k < c.getColliders().size(); ++k){
				rect = (Rectangle) c.getColliders().get(k).getShape();
				engine.getGraphicsEngine().getRenderer().drawRect(rect, c.getColliders().get(k).getType().getColor());
			}
			
		}
		
		
		return true;
	}
	
	protected ArrayList<Entity> getEntities(){
		return engine.getEntityManager().getEntities();
	}
	
	protected ArrayList<Component> getEntityComponents(ComponentId componentID){
		ArrayList<Component> comps = new ArrayList<Component>();
		for(int i = 0; i < getEntities().size(); ++i){
			for(Component c: getEntities().get(i).getComponents(componentID)){
				comps.add(c);
			}
		}
		return comps;
	}
	
	public boolean updateEntities(float dt){
		for(int i = 0; i < engine.getEntityManager().size(); ++i){
			Entity ent = engine.getEntityManager().getEntity(i);
			//if(engine.debug) Debug.Info.print(MessageType.PROPERTIES, ent.getName() + " aktiv:", ent.isActive());
			
			if(ent.isActive()){
				ent.state.update(dt);
			}
			else{
				engine.getEntityManager().removeEntity(ent);
			}
		}
		return true;
	}
	
	public EngineManager getEngine(){
		return engine;
	}
	
	public void setID(GameStateId ID){
		this.ID = ID;
	}
	public GameStateId getID(){
		return ID;
	}
}
