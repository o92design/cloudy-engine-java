package com.kioskars.ce.game;

import java.util.Stack;

import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.core.system.Subsystem;

public class GameStateMachine extends Subsystem{
	
	EngineManager engine;
	Stack<GameState> gameStates;
	
	public GameStateMachine(EngineManager engine){
		this.engine = engine;
	}
	
	public void update(float dt){
		gameStates.peek().update(dt);
	}
	
	public void render(){
		gameStates.peek().render();
	}
	
	public void pushState(GameState state){
		gameStates.push(state);
		gameStates.peek().onEnter();
	}
	
	public void changeState(GameState state){
		if(!gameStates.isEmpty())
		{
			if(gameStates.lastElement().getID() == state.getID()){
				return;
			}
			if(gameStates.peek().onExit()){
				gameStates.pop();
			}
			
		}
		
		pushState(state);
	}
	
	public void popState(){
		if(!gameStates.isEmpty()){
			if(gameStates.peek().onExit()){
				gameStates.pop();
			}
		}
	}

	public boolean initialize(String name, int ID) {
		gameStates = new Stack<GameState>();
		return true;
	}
}
