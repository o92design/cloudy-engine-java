package com.kioskars.ce.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.kioskars.ce.util.Debug;
import com.kioskars.ce.util.MessageType;

public class Keyboard implements KeyListener {

	private boolean[] isKeyDown;
	private boolean[] keysLast;
	public Command c;
	
	public Keyboard(int keys){
		isKeyDown = new boolean[keys];
		keysLast = new boolean[keys];
		for(int i = 0; i < isKeyDown.length; ++i){
			isKeyDown[i] = false;
			keysLast[i] = false;
		}
	}
	
	public void update(){
		keysLast = isKeyDown.clone();
	}
	
	public boolean isKey(int keyCode)
	{
		return isKeyDown[keyCode];
	}
	
	public boolean isKeyPressed(int keyCode)
	{
		return isKeyDown[keyCode] && !keysLast[keyCode];
	}
	
	public boolean isKeyReleased(int keyCode)
	{
		return !isKeyDown[keyCode] && keysLast[keyCode];
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		try{
			isKeyDown[e.getKeyCode()] = true;
		}catch(Exception exc){
			Debug.Error.print(MessageType.RUNNING, exc.toString());
			exc.printStackTrace();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		try{
			isKeyDown[e.getKeyCode()] = false;			
		}catch(Exception exc){
			Debug.Error.print(MessageType.RUNNING, exc.toString());
			exc.printStackTrace();
		}
	}
	
}
