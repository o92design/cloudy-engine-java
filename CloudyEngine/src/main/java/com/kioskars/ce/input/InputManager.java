package com.kioskars.ce.input;

import java.awt.event.KeyEvent;

import com.kioskars.ce.core.EngineManager;
import com.kioskars.ce.core.system.Subsystem;
import com.kioskars.ce.game.GameStateId;
import com.kioskars.ce.games.helga.MenuState;
import com.kioskars.ce.games.helga.PlayState;

public class InputManager extends Subsystem{
	private Keyboard keyboard;
	private Mouse mouse;
	
	public boolean initialize(String name, int ID) {
		setName(name);
		setID(ID);
		
		keyboard = new Keyboard(256);
		mouse = new Mouse();
		
		return true;
	}
	
	public void update(EngineManager engine){
		handleEngineInput(engine);
		keyboard.update();
	}
	
	
	public Command handleEngineInput(EngineManager engine){
		if(keyboard.isKeyPressed(KeyEvent.VK_F)) engine.getGameStateMachine().changeState(new PlayState(GameStateId.PLAY_STATE, engine));
		if(keyboard.isKeyPressed(KeyEvent.VK_G)) engine.getGameStateMachine().changeState(new MenuState(GameStateId.MENU_STATE, engine));
		if(keyboard.isKeyPressed(KeyEvent.VK_F2)) EngineManager.debug = !EngineManager.debug;
		
		return null;
	}

	public Keyboard getKeyboard() {
		return keyboard;
	}

	public Mouse getMouse() {
		return mouse;
	}
	
}
