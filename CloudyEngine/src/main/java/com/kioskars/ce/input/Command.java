package com.kioskars.ce.input;

import com.kioskars.ce.entity.Entity;

public interface Command {
	public abstract void execute(Entity entity);
}
